package com.xapo.xapotask.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GithubReposResponse(
        val items: List<GithubRepo>) : Serializable

data class GithubRepo(
        val id: Long,
        val name: String,
        @SerializedName("full_name")
        val fullName: String,
        val description: String,
        val language: String,
        @SerializedName("stargazers_count")
        val starsCount: Int,
        @SerializedName("forks")
        val forksCount: Int,
        @SerializedName("html_url")
        val url: String,
        val owner: Owner
) : Serializable

data class Owner(@SerializedName("avatar_url")
                 val avatarUrl: String) : Serializable