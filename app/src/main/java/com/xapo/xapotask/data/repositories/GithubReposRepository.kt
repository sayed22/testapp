package com.xapo.xapotask.data.repositories

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.xapo.xapotask.data.apis.githubrepos.GithubReposApiImpl
import com.xapo.xapotask.data.models.GithubRepo
import com.xapo.xapotask.data.models.GithubReposResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GithubReposRepository(val githubReposApiImpl: GithubReposApiImpl) {

    fun getRepos(queries: Map<String, String>): LiveData<List<GithubRepo>> {
        val liveData = MutableLiveData<List<GithubRepo>>()
        val call = githubReposApiImpl.getTrending(queries)

        call.enqueue(object : Callback<GithubReposResponse> {
            override fun onFailure(call: Call<GithubReposResponse>, t: Throwable) {
                liveData.value = null
            }

            override fun onResponse(call: Call<GithubReposResponse>, response: Response<GithubReposResponse>) {
                liveData.value = response.body()!!.items
            }
        })

        return liveData
    }
}